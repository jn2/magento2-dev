<?php

namespace Layout\Debugger\Model;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

/**
 * Class Layout
 * @package Layout\Debugger\Model
 */
class Layout implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * Layout constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     *
     * @return $this
     */
    public function execute(Observer $observer)
    {
        return $this;

        $xml = $observer->getEvent()->getLayout()->getXmlString();
        /*$this->_logger->debug($xml);*//*If you use it, check ouput string xml in var/debug.log*/
        $writer = new Stream(BP . '/var/log/layout_block.xml');
        $logger = new Logger();
        $logger->addWriter($writer);
        $logger->info($xml);
        return $this;
    }
}